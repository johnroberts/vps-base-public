# Purpose
[PasteOps](https://queue.acm.org/detail.cfm?id=3197520) VPS command framework, with a `vagrant` VM to act as `ansible` command machine. Two VMs:
- `command`
- `vps-mock`

`command` VM includes:
- A ZSH-based CLI accessible via `vagrant ssh`
- Preinstalled `ansible`
- Auto-generated per `vagrant provision` SSH keys to manage the `server` VM

`vps-mock` VM is a testbed to develop/try out roles, emulating a VPS host locally.

# Usage
- **Local development**:  prototype and test `ansible` roles locally, using the `command` and `server` VMs. The `server` mimics runs Ubuntu Server, and by default includes `docker`, `docker-compose`, and a passwordless sudo-capable `ansible` user that authenticates using SSH keys that are generated fresh per-Vagrant-provision.
- **Manage a remote VPS using Ansible**: use as an `ansible` CLI environment for managing software/applications on remote VPS hosts. The project includes some baseline roles for this purpose. Includes overview and copy-paste instructions for setting up the remote host to be managed as well.

Suggested to use Visual Studio code to work with this project, to get the benefits of auto `LF` newlines with this project and avoid some cross-platform issues with Windows.

## Local Development
On a host with `git`, `vagrant`, and `virtualbox` installed, `git clone` this repo,  cd to the project dir, then issue a `vagrant up` command. The first run will take a while as it downloads and installs/configures stuff.

### Basics
```
vagrant up
vagrant status # shows 2 running CMs: command and server created (with auto provision from Vagrantfile)
vagrant ssh command # during provisioning, a nice default CLI with ZSH and ansible based CLI is installed
```

This is now a ZSH-based CLI with:
- `ansible` pre-installed
- Some handy pre-populated files to control the server VM via `ssh` and `ansible`
- Access to the server under the names`vps-mock` or `server` via static hosts file mappings to reach the server VM at `192.168.33.20`
- An auto-generated SSH key (under `~/.ssh/generated_ed25519`) to control the server VM, using the `ansible` user defined on the server

#### Exploring the dev environment
After `vagrant ssh command` (or just `vagrant ssh` since `command` is the primary VM in the `Vagrantfile`):
```
# The server VM is at 192.168.33.20, there are entries for it in /etc/hosts:
cat /etc/hosts

# Test that you can connect to the server
ping -c 4 192.168.33.20
ping -c 4 server

# Look at the default SSH config
tree ~/.ssh

cat ~/.ssh/config

# Use the default ssh config to connect to server
#   - Uses the server's ansible user
#   - Uses ~/.ssh/config to define "server"
#   - Uses auto-generated SSH key in ~/.ssh/generated_ed25519
ssh server

exit # close SSH session with the server, back to command VM's CLI

# Poke around this directory -- it's the project's server-automation-directory, and it's meant as a directory to run ansible from
cd ~/server-automation/ansible

# Look at the ansible config; notice it uses the /vagrant mount to use the project's ansible roles and collections directories
cat ansible.cfg

# Look at the inventory; by default it uses the ansible user with generated SSH key
cat inventory

# Use ansible's ping module against the server to verify SSH and networking setup are proper
ansible -i inventory -m ping all

# Check out the playbook
cat server-playbook.yml

# Run the included playbook to apply it to the inventory
ansible-playbook -i inventory server-playbook.yml
```

By modifying the `server-automation/inventory` file, pointing to the right SSH keys for up a `sudo`-capable user on a remote host, you can manage any box that has SSH and Python using `ansible` this way!

#### Recreate the dev environment 
The base dev environment is idempotent, meaning it should be created the same way (with fresh SSH `ansible` user keypair) each time when you run (on the main host with `vagrant` and `virtualbox`):

```
rm generated_ed25519.pub # delete the previously generated SSH key
vagrant destroy
vagrant up
vagrant ssh
```

And you'll have a fresh dev environment to work with.

## Local Dev Environment Hints
### Steps
Try this workflow for an Ansible role edit/test cycle.

`vagrant ssh` to open `command` VM prompt, and `cd ~/server-automation/ansible`

This CLI (working directory set to `~/server-automation/ansible`) references the project's `server-automation/ansible/` directory. (This works because running `ansible` having a working directory of `~/server-automation/ansible/ansible.cfg`) allows referencing the project's `roles/` in `server-automation/ansible/server-playbook.yml`.

#### Test Connection
On `command` VM via `vagrant ssh command`:
```
# Tests that the ansible user can control server properly
cd ~/server-automation/ansible

ansible servername -i inventory -m ping

# Execute project's current playbook, which you can edit to add roles that are copied into the roles dir 
ansible-playbook -i inventory server-playbook.yml
```

#### Edit/Apply Roles
You can reference Ansible roles that are in the project's `roles/` directory inside the VM via `/vagrant/roles`, and the `ansible.cfg` in `~/server-automation/ansible` tells `ansible` to look there by default.

The net effect of that enables this workflow:

1. First, create/edit your ansible roles in the project's roles directory
2. Apply them to the server VM by editing server-playbook.yml to include the roles
3. On command VM: `cd ~/server-automation/ansible`
4. Apply the playbook against the server VM (pre-mapped via inventory under servername): `ansible-playbook -i inventory server-playbook.yml`
5. Then SSH to the server or do whatever to observe the results; modify roles/playbooks; rinse/repeat

## Manage Remote VPS
### On VPS Provider Cloud Console
Here's how to perform the first-time setup to let Ansible manage a remote Ubuntu box.

Install an OS from your cloud provider's web console using an Ubuntu template/ISO, noting the initial access credentials (like root password, SSH keys, etc.) in a password manager.

### On VPS Root Shell
Open an SSH session to control your server, and `sudo -s` to open a root shell.

On VPS root shell, set up the `ansible` service account:
- Create a user named `ansible` interactively with the `adduser` command
- Make that account capable of passwordless sudo. For this do `sudo visudo`, then add a line like `ansible ALL = (ALL) NOPASSWD: ALL` and save/quit the text editor
- Become the `ansible` user (via `su - ansible` from a root shell), and make sure ~/.ssh/authorized_keys exists and contains the public key corresponding to the key you would like to use with `ansible`. These can be generated on the `command` VM and saved in a password manager.

### On Command VM
Now you have everything you need to control the remote VPS via Ansible:
- VPS IP/hostname
- `ansible` service account on VPS capable of passwordless sudo
- The SSH private key trusted by the VPS's `ansible` account

Tell ansible about the remote VPS by editing `server-automation/ansible/inventory`. You'll need to change `ansible_host` to match the VPS, possibly changing `ansible_user` or `ansible_ssh_private_key_file` depending on the service account details you're using.
```
vps ansible_host=1.2.3.4 ansible_user=ansible ansible_ssh_private_key_file=/home/vagrant/.ssh/generated_ed25519
```

Now, use the `command` VM and `server-automation/ansible` directory to apply configuration to the remote VPS. For convenience this directory is pre-mapped under the `vagrant` user. So:
```
# Get shell in command VM
vagrant ssh command

# Change to the directory with the inventory file
cd ~/server-automation/ansible

# Apply configuration to the server with ansible
ansible-playbook -i inventory server-playbook.yml
```

Then edit `server-playbook.yml` to suit your needs (or write some more playbooks).

Bonus: `server-automation/ansible/ansible.cfg` includes roles/collections from the project directory, so you can use them in your playbooks.