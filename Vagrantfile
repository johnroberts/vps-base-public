# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  ##### VM 1: ansible command station #####
  config.vm.define "command", primary: true do |command|
    ##### BASE BOX #####
    command.vm.box = "ubuntu/jammy64"

    ##### NETWORKING #####
    command.vm.hostname = 'ansible-command'
    command.vm.network "private_network", ip: "192.168.33.10"

    ##### VIRTUALBOX CUSTOMIZATION ######
    command.vm.provider "virtualbox" do |v|
      v.gui = true
      v.memory = 1536

      # Bidirectional clipboard
      v.customize ["modifyvm", :id, "--clipboard", "bidirectional"]
    end

    ##### PROVISIONING VIA ANSIBLE ######
    command.vm.provision "ansible_local" do |ansible|
      ansible.playbook = "ansible-command-playbook.yml"
      ansible.install = true
      ansible.install_mode = :default
    end

    ##### FOLDER SHARING ######
    # Share directory intended to run `ansible` into /home/vagrant
    command.vm.synced_folder "server-automation/", "/home/vagrant/server-automation", mount_options: ["dmode=755,fmode=644"]

    # Windows-host specific workaround for ansible_local provisioner to use the project's ansible.cfg
    command.vm.synced_folder ".", "/vagrant",  mount_options: ["dmode=775,fmode=664"]
  end

  ##### VM 2: VPS server mock #####
  config.vm.define "server" do |server|
    ##### BASE BOX #####
    server.vm.box = "ubuntu/jammy64"

    ##### NETWORKING #####
    server.vm.hostname = 'server'
    server.vm.network "private_network", ip: "192.168.33.20"

    ##### VIRTUALBOX CUSTOMIZATION ######
    server.vm.provider "virtualbox" do |v|
      v.gui = true
      v.memory = 2048

      # Bidirectional clipboard
      v.customize ["modifyvm", :id, "--clipboard", "bidirectional"]
    end

    ##### PROVISIONING VIA ANSIBLE ######
    server.vm.provision "ansible_local" do |ansible|
      ansible.playbook = "server-mock-playbook.yml"
      ansible.install = true
      ansible.install_mode = :default
    end

    ##### FOLDER SHARING ######
    # Windows-host specific workaround for ansible_local provisioner to use the project's ansible.cfg
    server.vm.synced_folder ".", "/vagrant",  mount_options: ["dmode=775,fmode=664"]
  end
end
