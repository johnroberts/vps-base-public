# Future Ideas/Project TODOs
- Colorized shell prompts during VPS host setup based on dev/prod/enviroment (eg red for prod)
- Automate SSH and convenience stuffs
-   Auto-generate SSH keypair at `vagrant up` time on command VM with community openssh_keypair
-   Copy generated pubkey to server VM
-   Set up `~/.ssh/config` to have an entry using generated keypair to SSH to the server
- command VM `/etc/hosts` has an entry for server VM, now that there are static IPs
- on VPS install this ansible plugin to manage Docker Compose container building + state: community.docker.docker_compose

# Scratch dev notes
## command VM notes
(command VM swapped to a more regularly updated base box after this was written, but the functionality still exists)

The bento/ubuntu-18.04 base box works with `vagrant ssh`, but is released infrequently (so packages go out of date). So, `vars/user_vars.yml` has a `upgrade_apt_packages` variable to control whether apt packages should be upgraded or not (to speed up provisioning).


##  SSH keypair design ideas
This project generates ssh keypairs using `ssh-keygen` per provision.

Here are other ideas to manage SSH keys:
- Set up SSH CA using something like Hashicorp Vault
- Separate keypairs per project
- Separate keypairs per environment (dev/prod)
- Separate keypairs per cloud provider
- Separate keypair per host

## ssh keygen recommendations
### Security-focused
`ssh-keygen -t ed25519 -a 100`

More secure, less compatible (didn't work as Gitlab SSH key)

### Security + compatibility blend
`ssh-keygen -t ed25519`

### Compatibility-focused
`ssh-keygen -t rsa`

#### The Manual Commands/Notes From HostHatch
## Steps to prep HostHatch Ubuntu 18.04 for management via Ansible
### On Vagrant Command VM
- `vagrant up` for the `client-command-station` VM, then `vagrant ssh` and run subsequent commands from there
- Generate fresh SSH keys (to be used to authenticate `ansible` user): `ssh-keygen -t ed25519`; store these credentials in password manager

### On VPS stock Ubuntu 18.04 as root
- From `command` VM SSH to fresh Ubuntu host as root (to be disabled later) `ssh root@1.2.3.4`
- Change away from HostHatch generated PW; generate a new root password in password manager, and set that password interactively (`passwd`)
- Generate a password that will be used for `ansible` user account in password manager
- Create a sudo-capable `ansible` user
```
adduser ansible # follow interactive prompts, setting password to password manager-generated/stored PW
usermod -aG sudo ansible # make ansible user sudo-capable
```
- Don't require a sudo password for `ansible` user:
`visudo`, then add:

```
# Allow ansible to have passwordless sudo in sudoers file
ansible ALL = (ALL) NOPASSWD: ALL
```

### On command VM
- Add desired pubkey to /home/ansible/.ssh/authorized_keys: `ssh-copy-id -i ~/.ssh/id_ed25519.pub ansible@1.2.3.4`
- Test SSH connection: `ssh ansible@1.2.3.4`
- Create ansible inventory file: `mkdir ~/ansible && vim ~/ansible/inventory`
```
testserver ansible_host=1.2.3.4 ansible_user=ansible
```
- Test ansible against the host: `ansible -i ~/ansible/inventory -m ping testserver`